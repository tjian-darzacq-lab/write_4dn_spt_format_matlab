4DN format for Single-Particle Tracking (SPT) data - Matlab export code
--------------------------
this repository is maintained by Anders Sejr Hansen

# Export SPT data to 4DN format from Matlbb
This repository contains Matlab code for exporting SPT data to the
tab-delimited 4DN format. For full details on the 4DN SPT format,
please see this
![PDF](https://gitlab.com/tjian-darzacq-lab/Write_4DN_SPT_format_Matlab/4DN_SPT_format_PDF_description.pdf). 

## Overview
This repository contains a main script,
`Write_4DN_format_DataMerging.m`, which is used to read in and merge
data from multiple single-cell SPT experiments and a function
`Write_4DN_SPT_format.m`, which convert to the 4DN SPT format and
writes the corresponding `.spt` file. It's assumed that each
single-cell movie is analyzed separately and that the data is in the
`trackedPar` format that is also readable by
[Spot-On](https://gitlab.com/tjian-darzacq-lab/spot-on-matlab). Code
to analyze raw movies and which saves the resulting trajectories in
this format is also freely available
[SPT_LocAndTrack](https://gitlab.com/tjian-darzacq-lab/SPT_LocAndTrack). Briefly,
each single-cell MAT-file of trajectories should be stored in a
structured array called `trackedPar`, which contains the following
fields:
* `trackedPar`: structure array with length equal to the number of
trajectories. Each trajectory contains:
	* `trackedPar.xy`: Nx2 matrix of xy coordinates (double) in micrometers,
    where N is the trajectory length.
	* `trackedPar.Frame`: Nx1 column vector of the frame IDs (integers)
    of where the particle was located.
	* `trackedPar.TimeStamp`: Nx1 column vector of the time stamp (double)
    of when the particle was located in that frame.

## How to run
This repository comes with example data. The example data is raw SPT
data tracking Halo-CTCF in live U2OS nuclei. Please see the
corresponding
[Spot-On paper](https://elifesciences.org/articles/33125) for full
details. Here we will just use the data to illustrate how to merge
data from multiple single-cells within a single replicate and further
how to merge data from multiple biological replicates. Please follow
the steps outline below:

### Step 1 - fill out metadata and merge replicates
Open up `Write_4DN_format_DataMerging.m` and fill out the fields in
the `Header` structured array. You may need to add and/or remove
fields according to the details of your experiment (please see this
[PDF](https://gitlab.com/tjian-darzacq-lab/Write_4DN_SPT_format_Matlab/4DN_SPT_format_PDF_description.pdf)
for full details).
Then use the structured array, `struc_CTCF`, to fill out the
workspaces, path and name. This structure contains only a single
element in this example, but it should hopefully be clear how to add
additional elements in case a large number of files will need to be
merged.

### Step 2 - write out data in 4DN SPT format
Once you have filled out all the metadata in
`Write_4DN_format_DataMerging.m`, simple press `Run` in Matlab. The
code should take around 10-20 seconds to run and produce a file
`spt04_cond1_U2OS_Halo-CTCF_1ms_100Hz_PAJF646.spt`. All of the
processing takes place within the function `Write_4DN_SPT_format.m`,
which may have to be modified depending on the details of your
metadata. But it should hopefully be straightforward to modify it by
using the exisiting file as a template.

## Spot-On reads 4DN SPT format
To analyze SPT data in the 4DN format with
[web-version of Spot-On](https://spoton.berkeley.edu) please use the
[4DN SPT splitter](https://tjian-darzacq-lab.gitlab.io/Split_4DN_format/)
to split the merged 4DN file into SPT datasets for individual cells
(full code
[here](https://gitlab.com/tjian-darzacq-lab/Split_4DN_format) written
by Maxime Woringer).
This way, you can download any of the publically available 4DN SPT
datasets from the
[4DN Data Portal](https://data.4dnucleome.org/) and upload
them to [Spot-On](https://spoton.berkeley.edu) to analyze them. 

## Issues
If you find any bugs or have any suggestions for improvement, please
let me know. The code has been tested on Mac Matlab 2014b and may need
to be modified for other platforms or other versions of Matlab. 


## Acknowledgements
A big thanks to Maxime Woringer for spotting bugs in the initial
version of this code. The development of the 4DN SPT format code was
sustained collaboration between several people in the 4D Nucleome
network and beyond and more details who contributed can be found in
the 4DN format description.

A full list of 4D nucleome SPT datasets can be found at the
[4DN Data Portal](https://data.4dnucleome.org/). 
