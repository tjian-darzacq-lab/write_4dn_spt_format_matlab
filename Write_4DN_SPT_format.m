function [ finish ] = Write_4DN_SPT_format( SavePath, SaveName, LoadPath, WorkSpaceCellArray, Header, dT)
%WRITE_4DN_SPT_FORMAT save trackedPar data in the 4DN SPT format
tic;
disp(['processing ', SaveName]);
% generate a large matrix to store all of the data. To speed things up,
% it's best to pre-initialize memory and then delete any excess rows
% later. Guess how many rows by using 30k rows per file:
NumFiles = 0;
for FileIter = 1:length(WorkSpaceCellArray)
   NumFiles = NumFiles +  length(WorkSpaceCellArray);
end
GuessRowsPerFile = 30000;
DataColMatrix = zeros(round(GuessRowsPerFile*NumFiles), length(Header.DataColumns));

% Need to keep track of several indicies:
TotIdx = 1;
PrevRepIdx = 1;
PrevCellIdx = 1;

% loop over all Biological Replicates:
for RepIter = 1:length(WorkSpaceCellArray)
    disp(['processing replicate ', num2str(RepIter), ' of ', num2str(length(WorkSpaceCellArray))]);
    % loop over cells:
    for CellIter = 1:length(WorkSpaceCellArray{RepIter})
        % pre-initialize indices:
        TrajId = 1;

        curr_workspaces = WorkSpaceCellArray{RepIter};
        % now load in the current workspace:
        load([LoadPath, curr_workspaces{CellIter}]);

        % need to write in the following 5 columns:
        % 'Trajectory_ID', 't', 'X', 'Y', Frame_ID

        % loop through and write into the DataColumnMatrix:
        for iter = 1:length(trackedPar)
            DataColMatrix(TotIdx:(TotIdx-1+length(trackedPar(iter).Frame)), 1:5) = [TrajId*ones(length(trackedPar(iter).Frame),1), ... % trajectory ID
                                dT*trackedPar(iter).Frame, trackedPar(iter).xy(:,1), trackedPar(iter).xy(:,2), trackedPar(iter).Frame];
            % update TotIdx and TrajId:
            TrajId = TrajId + 1;
            TotIdx = TotIdx + length(trackedPar(iter).Frame);
        end
        % now you have finished a new cell, so need to fill in the
        % Cell_ID column:
        DataColMatrix(PrevCellIdx:(TotIdx-1),6) = CellIter.*ones(length(PrevCellIdx:(TotIdx-1)),1);
        % update the PrevCellIdx counter:
        PrevCellIdx = TotIdx;      
    end
    % OK, you have finished a biological replicate, so fill in this
    % part:
    DataColMatrix(PrevRepIdx:(TotIdx-1),7) = RepIter.*ones(length(PrevRepIdx:(TotIdx-1)),1);
    % update the PrevRepIdx counter:
    PrevRepIdx = TotIdx;
end
% remove excess rows from DataColMatrix
if size(DataColMatrix,1) > (TotIdx-1)
    DataColMatrix = DataColMatrix(1:(TotIdx-1),:);
end
tic;
% now add the color-channel:
DataColMatrix(:,8) = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% WRITE THE SPT FORMAT FILE %%%%%%%%%%%%%%%%%%%%%%%%
filename = fullfile([SavePath, SaveName]);
fid = fopen(filename,'w'); 

%%%% FILL IN REQUIRED HEADER FIELDS:
% write Header
fprintf(fid,'%s\n',['##',Header.version]);
% write name
fprintf(fid,'%s\n',['#',Header.name]);
% write TimeUnit
fprintf(fid,'%s\n',['#TimeUnit: ',Header.TimeUnit]);
% write XYZUnit
fprintf(fid,'%s\n',['#XYZUnit: ',Header.XYZUnit]);
% write TimeIncrement
fprintf(fid,'%s\n',['#TimeIncrement: ',Header.TimeIncrement]);

%%%% FILL IN OPTIONAL HEADER FIELDS ONLY IF THEY EXIST:
% ParticleLocalization_Algorithm_Name
if isfield(Header, 'ParticleLocalization_Algorithm_Name')
    fprintf(fid,'%s\n',['#ParticleLocalization_Algorithm_Name: ',Header.ParticleLocalization_Algorithm_Name]);
end
% ParticleLocalization_Algorithm_AuthorName
if isfield(Header, 'ParticleLocalization_Algorithm_AuthorName')
    fprintf(fid,'%s\n',['#ParticleLocalization_Algorithm_AuthorName: ',Header.ParticleLocalization_Algorithm_AuthorName]);
end
% ParticleLocalization_Algorithm_Publication
if isfield(Header, 'ParticleLocalization_Algorithm_Publication')
    fprintf(fid,'%s\n',['#ParticleLocalization_Algorithm_Publication: ',Header.ParticleLocalization_Algorithm_Publication]);
end
% ParticleLocalization_Algorithm_Description
if isfield(Header, 'ParticleLocalization_Algorithm_Description')
    fprintf(fid,'%s\n',['#ParticleLocalization_Algorithm_Description: ',Header.ParticleLocalization_Algorithm_Description]);
end
% ParticleLinking_Algorithm_Name
if isfield(Header, 'ParticleLinking_Algorithm_Name')
    fprintf(fid,'%s\n',['#ParticleLinking_Algorithm_Name: ',Header.ParticleLinking_Algorithm_Name]);
end
% ParticleLinking_Algorithm_AuthorName
if isfield(Header, 'ParticleLinking_Algorithm_AuthorName')
    fprintf(fid,'%s\n',['#ParticleLinking_Algorithm_AuthorName: ',Header.ParticleLinking_Algorithm_AuthorName]);
end
% ParticleLinking_Algorithm_Publication
if isfield(Header, 'ParticleLinking_Algorithm_Publication')
    fprintf(fid,'%s\n',['#ParticleLinking_Algorithm_Publication: ',Header.ParticleLinking_Algorithm_Publication]);
end
% ParticleLinking_Algorithm_Description
if isfield(Header, 'ParticleLinking_Algorithm_Description')
    fprintf(fid,'%s\n',['#ParticleLinking_Algorithm_Description: ',Header.ParticleLinking_Algorithm_Description]);
end
% MaxGaps
if isfield(Header, 'MaxGaps')
    fprintf(fid,'%s\n',['#MaxGaps: ',Header.MaxGaps]);
end
% Image_SizeC
if isfield(Header, 'Image_SizeC')
    fprintf(fid,'%s\n',['#Image_SizeC: ',Header.Image_SizeC]);
end
% Channel_ID
if isfield(Header, 'Channel_ID')
    fprintf(fid,'%s\n',['#Channel_ID: ',Header.Channel_ID]);
end
% Channel_1_Name
if isfield(Header, 'Channel_1_Name')
    fprintf(fid,'%s\n',['#Channel_1_Name: ',Header.Channel_1_Name]);
end
% Channel_1_Fluorophore
if isfield(Header, 'Channel_1_Fluorophore')
    fprintf(fid,'%s\n',['#Channel_1_Fluorophore: ',Header.Channel_1_Fluorophore]);
end
% Channel_1_Color
if isfield(Header, 'Channel_1_Color')
    fprintf(fid,'%s\n',['#Channel_1_Color: ',Header.Channel_1_Color]);
end
% Channel_1_ExcitationWavelength
if isfield(Header, 'Channel_1_ExcitationWavelength')
    fprintf(fid,'%s\n',['#Channel_1_ExcitationWavelength: ',Header.Channel_1_ExcitationWavelength]);
end
% Channel_1_EmissionWavelength
if isfield(Header, 'Channel_1_EmissionWavelength')
    fprintf(fid,'%s\n',['#Channel_1_EmissionWavelength: ',Header.Channel_1_EmissionWavelength]);
end            

%%%%%%%%%%%%%%%%%% WRITE THE ACTUAL DATA TO THE FILE %%%%%%%%%%%%%%%%%%
% print the data columns:
% need to pre-initialize based on the number of data columns:
%fprintf_descriptor = [repmat('%s\t ', 1, length(Header.DataColumns)), '\n'];
fprintf_descriptor = repmat('%s\t', 1, length(Header.DataColumns)); %make sure there is no added space
DataColumnsText = sprintf(fprintf_descriptor, Header.DataColumns{:});
% now write out the data columns:
fprintf(fid,fprintf_descriptor,DataColumnsText); 
fprintf(fid, '\n');

% close the file before writing the table:
fclose(fid);

%%%% WRITE OUT THE FULL DATA TABLE:
dlmwrite(filename,DataColMatrix,'-append','delimiter','\t', 'precision',10);


finish = 1; toc;
end

