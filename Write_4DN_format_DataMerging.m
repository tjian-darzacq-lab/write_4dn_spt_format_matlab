%   Write4DN_format.m
%   Anders Sejr Hansen, May 2018
clear; clc; close all;

%   DESCRIPTION
%   write out 4DN format to a tab delimited file

% Where to save the data:
SavePath = ['.', filesep, '4DN_SPT', filesep];

%   Define Header fields:
Header = struct();
Header.version = 'SPT format v1.0';
Header.name = 'test';
Header.TimeUnit = 's';
Header.XYZUnit = 'um';
Header.TimeIncrement = '0.01';
Header.ParticleLocalization_Algorithm_Name = 'MTT';
Header.ParticleLocalization_Algorithm_AuthorName = 'A Serge, N Bertaux, H Rigneault and D Marguet';
Header.ParticleLocalization_Algorithm_Publication = 'A Serge, N Bertaux, H Rigneault and D Marguet. Dynamic multiple-target tracing to probe spatiotemporal cartography of cell membranes. Nature methods. 2008 Aug;5(8):687. PMID 18604216; DOI: 10.1038/nmeth.1233';
Header.ParticleLocalization_Algorithm_Description = 'We used the MTT-algorithm and the following parameters:  Localization error: 10^(-6.25); deflation loops: 0; Blinking (frames): 1; max competitors: 3; max D (um^2/s): 20. The code is available at https://gitlab.com/tjian-darzacq-lab/SPT_LocAndTrack and full details are available with the Spot-On publication: Anders S Hansen*, Maxime Woringer*, Jonathan B Grimm, Luke D Lavis, Robert Tjian, Xavier Darzacq. Robust model-based analysis of single-particle tracking experiments with Spot-On. eLife, 2018, 7, e33125. doi: 10.7554/eLife.33125';
Header.ParticleLinking_Algorithm_Name = 'MTT';
Header.ParticleLinking_Algorithm_AuthorName = 'A Serge, N Bertaux, H Rigneault and D Marguet';
Header.ParticleLinking_Algorithm_Publication = 'A Serge, N Bertaux, H Rigneault and D Marguet. Dynamic multiple-target tracing to probe spatiotemporal cartography of cell membranes. Nature methods. 2008 Aug;5(8):687. PMID 18604216; DOI: 10.1038/nmeth.1233';
Header.ParticleLinking_Algorithm_Description = 'We used the MTT-algorithm and the following parameters:  Localization error: 10^(-6.25); deflation loops: 0; Blinking (frames): 1; max competitors: 3; max D (um^2/s): 20. The code is available at https://gitlab.com/tjian-darzacq-lab/SPT_LocAndTrack and full details are available with the Spot-On publication: Anders S Hansen*, Maxime Woringer*, Jonathan B Grimm, Luke D Lavis, Robert Tjian, Xavier Darzacq. Robust model-based analysis of single-particle tracking experiments with Spot-On. eLife, 2018, 7, e33125. doi: 10.7554/eLife.33125';
Header.MaxGaps = '1';
Header.Image_SizeC = '1'; % number of color channels - only one here.  
Header.Channel_ID = '1';
Header.Channel_1_Name = 'Halo-CTCF';
Header.Channel_1_Fluorophore = 'PA-JF646';
Header.Channel_1_Color = 'red';
Header.Channel_1_ExcitationWavelength = '633';
Header.Channel_1_EmissionWavelength = '664';
Header.DataColumns = {'#Columns: Trajectory_ID', 't', 'X', 'Y', 'Frame_ID', 'Cell_ID', 'BiologicalReplicate_ID', 'C'}; % headers of DataColumns
%%%%    remember to add "#Columns: " to the first header in DataColumns


% define Protein structures
struc_CTCF = struct();

dT = [9.977]; 
dye = {'646'};

%%%%%%%%% CTCF EXP A - PA-JF646 %%%%%%%%%
%   1. Exp A 646 1 ms
struc_CTCF(1).workspaces{1} = {'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep1_cell01', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep1_cell02', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep1_cell03', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep1_cell04'...
                                , 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep1_cell05', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep1_cell06', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep1_cell07', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep1_cell08'};                            
struc_CTCF(1).workspaces{2} = {'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep2_cell01', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep2_cell02', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep2_cell03',...
                            'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep2_cell04', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep2_cell05'};
struc_CTCF(1).workspaces{3} = {'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep3_cell01', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep3_cell02', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep3_cell03',...
                            'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep3_cell04', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep3_cell05'};
struc_CTCF(1).workspaces{4} = {'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep4_cell01', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep4_cell02', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep4_cell03', 'U2OS_C32_Halo-CTCF_PA-JF646_1ms-633nm_100Hz_rep4_cell04'};
struc_CTCF(1).path = ['.', filesep, 'ExampleData', filesep];
struc_CTCF(1).name = 'spt04_cond1_U2OS_Halo-CTCF_1ms_100Hz_PAJF646.spt';

% compile SPT datasets:
for iter = 1:length(struc_CTCF)
    curr_dT = dT(iter)/1000;
    Header.TimeIncrement = num2str(curr_dT);
    curr_dye = dye{iter};
    if strcmp(curr_dye, '646')
        Header.Channel_1_Fluorophore = 'PA-JF646';
        Header.Channel_1_Color = 'red';
        Header.Channel_1_ExcitationWavelength = '633';
        Header.Channel_1_EmissionWavelength = '664';
    elseif strcmp(curr_dye, '549')
        Header.Channel_1_Fluorophore = 'PA-JF549';
        Header.Channel_1_Color = 'Orange';
        Header.Channel_1_ExcitationWavelength = '561';
        Header.Channel_1_EmissionWavelength = '580';    
    end
    
    % process CTCF:
    Header.Channel_1_Name = 'Halo-CTCF';
    Header.name = struc_CTCF(iter).name;
    [ ~ ] = Write_4DN_SPT_format( SavePath, struc_CTCF(iter).name , struc_CTCF(iter).path, struc_CTCF(iter).workspaces, Header, curr_dT );
    
 
end
